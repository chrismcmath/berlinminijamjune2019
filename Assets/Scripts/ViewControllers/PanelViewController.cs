﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PanelViewController : MonoBehaviour
{
    public GameObject Highlight;
    public GameObject Red;
    public GameObject Green;
    public GameObject Blue;
    public GameObject Level_1;
    public GameObject Level_2;
    public GameObject Level_3;
    public GameObject RegularView;
    public GameObject BrokenView;
    
    public GridPosition Position;
    SolarPanel _PanelModel;
    PlayerController _PlayerController;
    Material _IndicatorMaterial;
    CrystalSpawnController _CrystalSpawnController;

    void Awake()
    {
        Position = new GridPosition {X = int.Parse(name[0].ToString()), Y = int.Parse(name[1].ToString())};
    }

    void Start()
    {
        _CrystalSpawnController = FindObjectOfType<CrystalSpawnController>();
        _PanelModel = FindObjectOfType<Model>().Grid.Panels[Position];
        _PlayerController = FindObjectOfType<PlayerController>();
        _PanelModel.OnSolarPanelChanged += OnSolarPanelChanged;
        _IndicatorMaterial = Highlight.GetComponent<Renderer>().material;
        
        UpdateEverything();
    }

    void Update()
    {
        bool hasHighlight = _PlayerController.Position.Equals(Position);
        Highlight.SetActive(hasHighlight);

        if (hasHighlight)
        {
            _IndicatorMaterial.SetColor("_EmissionColor", GetColourFromCrystalType(_CrystalSpawnController.NextCrystalType));
        }
    }

    Color GetColourFromCrystalType(Crystal.CrystalType nextCrystalType)
    {
        switch (nextCrystalType)
        {
            case Crystal.CrystalType.NULL:
                break;
            case Crystal.CrystalType.RED:
                return Color.red;
            case Crystal.CrystalType.GREEN:
                return Color.green;
            case Crystal.CrystalType.BLUE:
                return Color.blue;
            default:
                throw new ArgumentOutOfRangeException(nameof(nextCrystalType), nextCrystalType, null);
        }
        return Color.black;
    }

    void OnSolarPanelChanged()
    {
        UpdateEverything();
    }

    void UpdateEverything()
    {
        if (_PanelModel.State == SolarPanel.SolarPanelState.FAILED)
        {
            BrokenView.SetActive(true);
            RegularView.SetActive(false);
            return;
        }
        else
        {
            BrokenView.SetActive(false);
            RegularView.SetActive(true);
        }
        
        Level_1.SetActive(_PanelModel.Crystals.Count >= 1);
        Level_2.SetActive(_PanelModel.Crystals.Count >= 2);
        Level_3.SetActive(_PanelModel.Crystals.Count >= 3);
        
        Red.SetActive(_PanelModel.HasCrystal(Crystal.CrystalType.RED));
        Green.SetActive(_PanelModel.HasCrystal(Crystal.CrystalType.GREEN));
        Blue.SetActive(_PanelModel.HasCrystal(Crystal.CrystalType.BLUE));
    }
}
