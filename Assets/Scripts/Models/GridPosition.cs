using System;

[Serializable]
public struct GridPosition : IEquatable<GridPosition>
{
    public int X;
    public int Y;

    public override string ToString()
    {
        return string.Format("({0}, {1})", X, Y);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is GridPosition other && Equals(other);
    }

    public bool Equals(GridPosition other)
    {
        return X == other.X && Y == other.Y;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (X * 397) ^ Y;
        }
    }
}