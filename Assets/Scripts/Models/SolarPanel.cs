﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SolarPanel
{
    public const float RECOVERY_SPEED = 16f;
    
    public event Action OnSolarPanelChanged = delegate {};
    public static event Action OnSolarFilled = delegate { };
    public static event Action OnSolarBlownUp = delegate { };

    public enum SolarPanelState
    {
        NULL,
        INCOMPLETE,
        COMPLETE,
        FAILED
    }
    
    public SolarPanelState State;
    public List<Crystal> Crystals = new List<Crystal>();
    
    float _LastStateChange;

    public void AddCrystal(Crystal crystal)
    {
        if (HasCrystal(crystal))
        {
            ChangeState(SolarPanelState.FAILED);
            OnSolarBlownUp();
            return;
        }
        
        Crystals.Add(crystal);
        
        if (Crystals.Count == 3)
        {
            Crystals.Clear();
            OnSolarFilled();
        }
        
        OnSolarPanelChanged();
    }

    public void Update()
    {
        if (State != SolarPanelState.FAILED)
        {
            return;
        }

        if (_LastStateChange + RECOVERY_SPEED < Time.realtimeSinceStartup)
        {
            Crystals.Clear();
            ChangeState(SolarPanelState.INCOMPLETE);
        }
    }

    void ChangeState(SolarPanelState state)
    {
        _LastStateChange = Time.realtimeSinceStartup;
        State = state;

        OnSolarPanelChanged();
    }

    public bool HasCrystal(Crystal.CrystalType crystalType)
    {
        for (int i = 0; i < Crystals.Count; i++)
        {
            if (Crystals[i].Type == crystalType)
            {
                return true;
            }
        }
        
        return false;
    }

    public bool HasCrystal(Crystal crystal)
    {
        return HasCrystal(crystal.Type);
    }

}
