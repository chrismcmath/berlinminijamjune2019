﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalSpawner : MonoBehaviour
{

    [SerializeField]
    private GameObject crystalPrefab;
    public bool testMode;
    public Transform WhaleRoot;

    [SerializeField]
    private Transform testTarget;

    [SerializeField]
    private Color testColor;

    private Transform spawnPosition;

    PlayerController playerController;


    //private void Start()
    //{

    //    if (repeatForTesting)
    //        InvokeRepeating("SpawnCrystal", 1f, 2f);
    //}

    void Awake()
    {
        FindObjectOfType<CrystalSpawnController>().OnCrystalSpawned += OnCrystalSpawned;
        playerController = FindObjectOfType<PlayerController>();
    }

    void OnCrystalSpawned(Crystal.CrystalType crystalType)
    {
        SpawnCrystal(GetTarget(playerController.Position), crystalType);
    }

    Transform GetTarget(GridPosition playerControllerPosition)
    {
        return GameObject.Find(string.Format("{0}{1}", playerControllerPosition.X, playerControllerPosition.Y)).transform;
    }

    public void SpawnCrystal(Transform target, Crystal.CrystalType color)
    {
        switch (color)
        {
            case Crystal.CrystalType.NULL:
                break;
            case Crystal.CrystalType.RED:
                SpawnCrystal(target, Color.red);
                break;
            case Crystal.CrystalType.GREEN:
                SpawnCrystal(target, Color.green);
                break;
            case Crystal.CrystalType.BLUE:
                SpawnCrystal(target, Color.blue);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(color), color, null);
        }
    }
    public void SpawnCrystal(Transform target, Color color)
    {
        FlyingCrystal newCrystal = Instantiate(crystalPrefab, transform.position, Quaternion.identity).GetComponent<FlyingCrystal>();

        if (testMode)
        {
        }

        newCrystal.Spawn(target, color);
    }

        public void TestSpawn()
    {

            SpawnCrystal(testTarget, testColor);
    }
}
