using UnityEngine;

public class Model : MonoBehaviour
{
    public Grid Grid;

    void Awake()
    {
        Grid = new Grid();
        Grid.ResetGrid();
    }

    void Update()
    {
        Grid.Update();
    }
}