﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingCrystal : MonoBehaviour
{
    

    private ParticleSystem.MainModule particles;
    private ParticleSystem.EmissionModule emission;

    [SerializeField]
    private float speed;

    [SerializeField]
    private Vector3 offSetBehindCamera;

    private Transform target;




    private Vector3 movementVector = Vector3.zero;

 

   

    private void Awake()
    {
        particles = GetComponent<ParticleSystem>().main;
        emission = GetComponent<ParticleSystem>().emission;
    }

    public void Spawn(Transform newTarget, Color color)
    {
        target = newTarget;
        movementVector = (target.position - transform.position).normalized * speed;
        transform.LookAt(target);
        particles.startColor = color;
    }


    private void Update()
    {
        //transform.position += movementVector * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        if(transform.position == target.position)
        {
            emission.rateOverTime = 0f;
            //Destroy(gameObject, 1f);
        }

    }




}
