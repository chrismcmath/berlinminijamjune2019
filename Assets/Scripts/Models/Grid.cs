using System.Collections.Generic;
using UnityEngine;

public class Grid
{
    public const int WIDTH = 4;
    public const int HEIGHT = 2;
    
    public Dictionary<GridPosition, SolarPanel> Panels = new Dictionary<GridPosition, SolarPanel>();

    public void AddCrystal(Crystal crystal, GridPosition gridPosition)
    {
        //Debug.LogFormat("add {0} to {1}", crystal.Type, gridPosition);
        Panels[gridPosition].AddCrystal(crystal);
    }

    public void ResetGrid(int width = WIDTH, int height = HEIGHT)
    {
        Panels.Clear();
        for (int w = 0; w < width; w++)
        {
            for (int h = 0; h < height; h++)
            {
                GridPosition gp = new GridPosition {X = w, Y = h};
                Panels.Add(gp, new SolarPanel());
            }
        }
    }

    public void Update()
    {
        foreach (KeyValuePair<GridPosition, SolarPanel> panel in Panels)
        {
            panel.Value.Update();
        }
    }
}