using UnityEngine;

public class Crystal
{
    public enum CrystalType
    {
        NULL,
        RED,
        GREEN,
        BLUE
    }

    public CrystalType Type;
}