using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CrystalSpawnController : MonoBehaviour
{
    public event Action<Crystal.CrystalType> OnCrystalSpawned = delegate {};
    
    public const float SPAWN_RATE = 2f;
    public const int COMPLEXITY = 2;
    
    public enum CrystalSpawnState
    {
        WAITING,
        SPAWNING
    }

    public CrystalSpawnState State;
    public List<Crystal.CrystalType> Bag = new List<Crystal.CrystalType>();
    
    float _LastStateChange;
    
    Crystal.CrystalType _NextCrystalType = Crystal.CrystalType.RED;
    public Crystal.CrystalType NextCrystalType
    {
        get { return _NextCrystalType; }
    }

    void Update()
    {
        switch (State)
        {
            case CrystalSpawnState.WAITING:
                if (_LastStateChange + SPAWN_RATE < Time.realtimeSinceStartup)
                {
                    ChangeState(CrystalSpawnState.SPAWNING);
                }
                break;
            case CrystalSpawnState.SPAWNING:
                OnCrystalSpawned(_NextCrystalType);
                //Debug.LogFormat("spawn next {0}", _NextCrystalType);
                ChangeState(CrystalSpawnState.WAITING);
                _NextCrystalType = GetNextCrystalType();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    void RefillBag()
    {
        Bag.Clear();
        for (int i = 0; i < COMPLEXITY; i++)
        {
            Bag.Add(Crystal.CrystalType.RED);
            Bag.Add(Crystal.CrystalType.GREEN);
            Bag.Add(Crystal.CrystalType.BLUE);
        }
    }

    Crystal.CrystalType GetNextCrystalType()
    {
        if (Bag.Count == 0)
        {
            RefillBag();
        }
        
        int index = Random.Range(0, Bag.Count);
        Crystal.CrystalType crystalType = Bag[index];
        Bag.RemoveAt(index);
        return crystalType;
    }
    
    void ChangeState(CrystalSpawnState state)
    {
        _LastStateChange = Time.realtimeSinceStartup;
        State = state;
    }
}