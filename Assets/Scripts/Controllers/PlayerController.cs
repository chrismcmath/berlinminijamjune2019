﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public const float MOVE_SPEED = 0.001f;
    public const float FIRING_SPEED = 0.1f;
    
    public float MovementThreshold = 0.1f;
    
    public enum PlayerState
    {
        IDLE,
        MOVING,
        FIRING
    }

    public PlayerState State;
    public GridPosition Position;
    float _LastStateChange;
    Model _Model;
    Crystal.CrystalType _NextCrystalType;

    void Awake()
    {
        GetComponent<CrystalSpawnController>().OnCrystalSpawned += OnCrystalSpawned;
        _Model = GetComponent<Model>();
    }

    void OnCrystalSpawned(Crystal.CrystalType type)
    {
        _NextCrystalType = type;
        ChangeState(PlayerState.FIRING);
    }

    void Update()
    {
        //Debug.LogFormat("GridPosition: {0}", Position);
        switch (State)
        {
            case PlayerState.IDLE:
                UpdateIdle();
                break;
            case PlayerState.MOVING:
                UpdateMoving();
                break;
            case PlayerState.FIRING:
                UpdateFiring();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    void UpdateFiring()
    {
        if (_LastStateChange + FIRING_SPEED < Time.realtimeSinceStartup)
        {
            _Model.Grid.AddCrystal(new Crystal {Type = _NextCrystalType}, Position);
            ChangeState(PlayerState.IDLE);
        }
    }

    void UpdateMoving()
    {
        if (_LastStateChange + MOVE_SPEED < Time.realtimeSinceStartup)
        {
            ChangeState(PlayerState.IDLE);
        }
    }

    void UpdateIdle()
    {
        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");
        
        if (Input.GetKeyDown("left"))
        {
            MoveHorizontal(-1);
        }

        if (Input.GetKeyDown("right"))
        {
            MoveHorizontal(1);
        }
        
        if (Input.GetKeyDown("down"))
        {
            MoveVertical(-1);
        }

        if (Input.GetKeyDown("up"))
        {
            MoveVertical(1);
        }
    }

    void MoveHorizontal(int delta)
    {
        int nextPoint = Position.X;

        while (TryGetNextPoint(delta, Grid.WIDTH, ref nextPoint))
        {
            GridPosition candidatePosition = new GridPosition {X = nextPoint, Y = Position.Y};
            if (IsFailed(candidatePosition))
            {
                continue;
            }
            
            Position.X = nextPoint;
            break;
        }
    }

    bool IsFailed(GridPosition candidatePosition)
    {
        return _Model.Grid.Panels[candidatePosition].State == SolarPanel.SolarPanelState.FAILED;
    }

    bool TryGetNextPoint(int delta, int max, ref int nextPoint)
    {
        int current = nextPoint;
        nextPoint += delta;
        nextPoint = nextPoint < 0 ? 0 : nextPoint;
        nextPoint = nextPoint >= max ? max - 1 : nextPoint;
        return current != nextPoint;
    }
    
    void MoveVertical(int delta)
    {
        int nextPoint = Position.Y;

        while (TryGetNextPoint(delta, Grid.HEIGHT, ref nextPoint))
        {
            GridPosition candidatePosition = new GridPosition {X = Position.X, Y = nextPoint};
            if (IsFailed(candidatePosition))
            {
                continue;
            }
            
            Position.Y = nextPoint;
            break;
        }
    }

    void ChangeState(PlayerState state)
    {
        //Debug.LogFormat("change player state to {0}", state);
        _LastStateChange = Time.realtimeSinceStartup;
        State = state;
    }
}
