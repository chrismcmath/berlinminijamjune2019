﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{ 
    [SerializeField]
    private int maxFails;

    [SerializeField]
    private TextMeshProUGUI scoreText;

    private int score, currentFails;



    private void Start()
    {
        ListenToAllPanels();
    }

    private void ListenToAllPanels()
    {
        SolarPanel.OnSolarBlownUp += SolarPanelblownUp;
        SolarPanel.OnSolarFilled += SolarPanelFilled;
    }

    public void NewRound()
    {
        currentFails = 0;
        score = 0;
        scoreText.text = score.ToString();
    }


    private void SolarPanelblownUp()
    {
        currentFails ++;

        /* Remove game over state
        if(currentFails == maxFails)
            GameOver();
            */
    }

    private void OnDestroy()
    {
        SolarPanel.OnSolarBlownUp -= SolarPanelblownUp;
        SolarPanel.OnSolarFilled -= SolarPanelFilled;
    }

    private void SolarPanelFilled()
    {
        score++;
        scoreText.text = score.ToString();
    }


    private void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
