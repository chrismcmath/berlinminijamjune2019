﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorButtons : MonoBehaviour
{

    [CustomEditor(typeof(CrystalSpawner))]
    public class HackerButtons : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("SpawnCrystal"))
            {
                (target as CrystalSpawner).TestSpawn();
            }
        }


    }
}
